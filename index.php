<?php
    require_once('animal.php');
    require_once('Ape.php');
    require_once('Frog.php');

    $sheep = new Animal("shaun");
    echo "Nama : ".$sheep->name ."<br>"; // "shaun"
    echo "Legs : ".$sheep->legs ."<br>"; // 4
    echo "Cold blooded : ".$sheep->cold_blooded ."<br> <br>"; // "no" 

    $kodok = new Frog("buduk");
    echo "Nama : ".$kodok->name ."<br>"; // "buduk"
    echo "Legs : ".$kodok->legs ."<br>"; // 4
    echo "Cold blooded : ".$kodok->cold_blooded ."<br>"; // "no" 

    echo $kodok->jump() ."<br> <br>"; // "hop hop"

    $sungokong = new Ape("kera sakti");
    echo "Nama : ".$sungokong->name ."<br>"; // "kera sakti"
    echo "Legs : ".$sungokong->legs ."<br>"; // 2
    echo "Cold blooded : ".$sungokong->cold_blooded ."<br>"; // "no"
    
    echo $sungokong->yell() // "Auooo"



?>